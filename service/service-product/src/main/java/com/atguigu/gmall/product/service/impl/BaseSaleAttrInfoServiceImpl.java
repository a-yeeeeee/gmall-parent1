package com.atguigu.gmall.product.service.impl;

import com.atguigu.gmall.model.product.BaseSaleAttr;
import com.atguigu.gmall.product.mapper.BaseSaleAttrInfoMapper;
import com.atguigu.gmall.product.service.BaseSaleAttrInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author aye_quq
 * @create 2021-08-05 19:36
 */
@Service
public class BaseSaleAttrInfoServiceImpl extends ServiceImpl<BaseSaleAttrInfoMapper,BaseSaleAttr> implements BaseSaleAttrInfoService {
}

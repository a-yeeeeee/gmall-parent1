package com.atguigu.gmall.product.service;

import com.atguigu.gmall.model.product.SpuInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

/**
 * @author aye_quq
 * @create 2021-08-05 13:35
 */
public interface SpuInfoService extends IService<SpuInfo> {
    void saveSpuInfo(SpuInfo spuInfo);
}

package com.atguigu.gmall.product.service;

import com.atguigu.gmall.model.product.BaseTrademark;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

/**
 * @author aye_quq
 * @create 2021-08-03 22:07
 */
public interface BaseTrademarkService  extends IService<BaseTrademark> {

    IPage<BaseTrademark> getPage(Page<BaseTrademark> pageParam);

    void deleteTeadeMarkById(Long id);
}

package com.atguigu.gmall.product.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.model.product.SpuImage;
import com.atguigu.gmall.model.product.SpuInfo;
import com.atguigu.gmall.model.product.SpuSaleAttr;
import com.atguigu.gmall.product.mapper.SpuSaleAttrMapper;
import com.atguigu.gmall.product.service.SpuImageService;
import com.atguigu.gmall.product.service.SpuInfoService;
import com.atguigu.gmall.product.service.SpuSaleAttrService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author aye_quq
 * @create 2021-08-05 13:33
 */
@Api(tags = "spu接口")
@RestController
@RequestMapping("/admin/product")
public class SpuController {
    @Autowired
    private SpuInfoService spuInfoService;
    @Autowired
    private SpuImageService spuImageService;
    @Autowired
    private SpuSaleAttrService spuSaleAttrService;

    @ApiOperation("spu分页查询")
    @GetMapping("{page}/{limit}")
    private Result spuInfoPageList(
            @RequestParam("category3Id") Long category3Id,
            @PathVariable("page") Long page,
            @PathVariable("limit") Long limit
    ) {
        Page<SpuInfo> page1 = new Page<SpuInfo>(page, limit);
        QueryWrapper<SpuInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("category3_id", category3Id);

        Page<SpuInfo> infoPage = spuInfoService.page(page1, wrapper);
        return Result.ok(infoPage);
    }

    @ApiOperation(value = "保存spu信息")
    @PostMapping("/saveSpuInfo")
    public Result saveSpuInfo(@RequestBody SpuInfo spuInfo) {
        spuInfoService.saveSpuInfo(spuInfo);
        return Result.ok();
    }

    @ApiOperation(value = "根据spuId获取图片列表")
    @GetMapping("/spuImageList/{spuId}")
    public Result<List<SpuImage>> getImageList(@PathVariable("spuId") Long spuId) {
//        QueryWrapper<SpuImage> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("spu_id", spuId);
//        List<SpuImage> list = spuImageService.list(queryWrapper);
//        return Result.ok(list);
        QueryWrapper<SpuImage> wrapper = new QueryWrapper<>();
        wrapper.eq("spu_id",spuId);

        List<SpuImage> list = spuImageService.list(wrapper);
        return Result.ok(list);

    }

    @ApiOperation(value = "根据spuid获取销售属性")
    @GetMapping("spuSaleAttrList/{spuId}")
    public Result getSpuSaleAttrById(@PathVariable Long spuId) {
        List<SpuSaleAttr> list =spuSaleAttrService.getSpuSaleAttrById(spuId);
        return Result.ok(list);
    }
}

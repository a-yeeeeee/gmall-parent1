package com.atguigu.gmall.product.mapper;

import com.atguigu.gmall.model.product.SkuInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author aye_quq
 * @create 2021-08-06 18:50
 */
@Mapper
public interface SkuInfoMapper extends BaseMapper<SkuInfo> {
    void updateSkuSaleStatus(Long skuId, int i);
}

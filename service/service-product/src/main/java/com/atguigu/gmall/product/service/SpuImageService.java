package com.atguigu.gmall.product.service;

import com.atguigu.gmall.model.product.SpuImage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author aye_quq
 * @create 2021-08-05 21:04
 */
public interface SpuImageService extends IService<SpuImage> {
}

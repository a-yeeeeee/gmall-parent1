package com.atguigu.gmall.product.service;

import com.atguigu.gmall.model.product.BaseAttrValue;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author aye_quq
 * @create 2021-08-03 11:00
 */
public interface BaseAttrValueService extends IService<BaseAttrValue> {
}

package com.atguigu.gmall.product.component;

import com.atguigu.gmall.product.config.MinioConfig;

import io.minio.MinioClient;
import io.minio.PutObjectOptions;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.UUID;

/**
 * @author aye_quq
 * @create 2021-08-04 11:21
 */
@Component
public class MinioService {
//    @Autowired
//    MinioClient minioClient;

    @Autowired
    MinioConfig minioConfig;


    public String upload(InputStream inputStream, String bucketName, String fileName) throws Exception{
        MinioClient minioClient = minioConfig.minioClient();
        if(StringUtils.isEmpty(bucketName)){
            //给默认位置上传
            bucketName = minioConfig.getDefaultBucket();
        }else{
            if(!minioClient.bucketExists(bucketName)){
                minioClient.makeBucket(bucketName);
            }
        }
        fileName = UUID.randomUUID().toString().replace("-","")+"_"+fileName;
        PutObjectOptions options = new PutObjectOptions(inputStream.available(),-1);
        minioClient.putObject(bucketName,fileName,inputStream,options);

        //返回整个文件路径？？？
        //URL+bucket+filename
        String url = "";
        url = minioConfig.getUrl() + "/"+bucketName + "/"+fileName;
        return url;//文件在minio的存储路径
    }
}


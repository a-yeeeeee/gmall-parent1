package com.atguigu.gmall.product.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.common.result.ResultCodeEnum;
import com.atguigu.gmall.model.product.BaseCategory1;
import com.atguigu.gmall.model.product.BaseCategory2;
import com.atguigu.gmall.model.product.BaseCategory3;
import com.atguigu.gmall.product.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author aye_quq
 * @create 2021-08-01 15:42
 */
@Api(tags = "分类与平台属性接口")
@RestController
@RequestMapping("/admin/product")
public class CategoryAdminController {
    @Autowired
    private CategoryService categoryService;


    @ApiOperation("获取一级目录")
    @GetMapping("/getCategory1")
    public Result<List<BaseCategory1>> getCategory1() {
        List<BaseCategory1> category1s = categoryService.getCategory1();
        return Result.ok(category1s);
    }

    @ApiOperation("获取二级目录")
    @GetMapping("/getCategory2/{category1Id}")
    public Result<List<BaseCategory2>> getCategory2(
            @ApiParam(value = "一级分类id", required = true)
            @PathVariable Long category1Id) {
        if (category1Id > 0){

            List<BaseCategory2> category2s = categoryService.getCategory2(category1Id);
            return Result.ok(category2s);
        }else {
            return Result.build(null, ResultCodeEnum.SECKILL_ILLEGAL);
        }
    }

    @ApiOperation("获取三级目录")
    @GetMapping("/getCategory3/{category2Id}")
    public Result<List<BaseCategory3>> getCategory3(
            @ApiParam(value = "二级分类id", required = true)
            @PathVariable Long category2Id) {
        List<BaseCategory3> category3s = categoryService.getCategory3(category2Id);
       return Result.ok(category3s);
    }

}

package com.atguigu.gmall.product.service;

import com.atguigu.gmall.model.product.SkuSaleAttrValue;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author aye_quq
 * @create 2021-08-06 19:59
 */
public interface SkuSaleAttrValueService extends IService<SkuSaleAttrValue> {
}

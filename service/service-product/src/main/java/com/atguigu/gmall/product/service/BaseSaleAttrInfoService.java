package com.atguigu.gmall.product.service;

import com.atguigu.gmall.model.product.BaseSaleAttr;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.jnlp.IntegrationService;

/**
 * @author aye_quq
 * @create 2021-08-05 19:23
 */
public interface BaseSaleAttrInfoService extends IService<BaseSaleAttr> {
}


package com.atguigu.gmall.product.service;

import com.atguigu.gmall.model.product.SkuInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author aye_quq
 * @create 2021-08-06 18:48
 */
public interface SkuInfoService extends IService<SkuInfo> {
    boolean saveSkuInfo(SkuInfo skuInfo);

    boolean onSale(Long skuId);

    boolean cancelSale(Long skuId);
}

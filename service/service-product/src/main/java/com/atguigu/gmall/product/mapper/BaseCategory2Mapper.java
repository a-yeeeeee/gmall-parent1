package com.atguigu.gmall.product.mapper;

import com.atguigu.gmall.model.product.BaseCategory1;
import com.atguigu.gmall.model.product.BaseCategory2;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.annotation.MapperScan;

/**
 * @author aye_quq
 * @create 2021-07-31 16:29
 */
@Mapper
public interface BaseCategory2Mapper extends BaseMapper<BaseCategory2> {
}

package com.atguigu.gmall.product.config;

import io.minio.MinioClient;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author aye_quq
 * @create 2021-08-04 11:12
 */
@ConfigurationProperties(prefix = "minio")
@Configuration
@Data
public class MinioConfig {

    private String url;
    private String accessKey;
    private String secretKey;
    private String defaultBucket;


    @Bean
    public MinioClient minioClient(){
        try {
            MinioClient minioClient = new MinioClient(url,accessKey, secretKey);
            boolean exists = minioClient.bucketExists(defaultBucket);
            if(!exists){
                minioClient.makeBucket(defaultBucket);
            }
            return minioClient;
        } catch (Exception e) {
            return null;
        }
    }
}

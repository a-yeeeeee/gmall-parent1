package com.atguigu.gmall.product;

import com.atguigu.gmall.common.config.MybatisPlusConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

/**
 * @author aye_quq
 * @create 2021-07-31 16:07
 */
//@SpringBootApplication
//@ComponentScan("com.atguigu.gmall")
//@EnableDiscoveryClient
@SpringBootApplication
@Import(MybatisPlusConfig.class)
public class ServiceProductApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceProductApplication.class,args);
    }
}

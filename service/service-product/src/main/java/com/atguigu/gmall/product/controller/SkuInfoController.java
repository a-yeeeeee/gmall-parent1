package com.atguigu.gmall.product.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.model.product.SkuInfo;
import com.atguigu.gmall.product.service.SkuInfoService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.ValidationEvent;

/**
 * @author aye_quq
 * @create 2021-08-05 22:43
 */
@Api(tags = "sku属性方法")
@RestController
@RequestMapping("/admin/product")
public class SkuInfoController {
   @Autowired
   private SkuInfoService skuInfoService;

    @ApiOperation(value = "保存sku属性")
    @PostMapping("/saveSkuInfo")
    public Result saveSkuInfo(
            @ApiParam(value = "skuInfo",required = true)
            @RequestBody SkuInfo skuInfo){
        boolean b = skuInfoService.saveSkuInfo(skuInfo);
        return Result.ok();
    }
    @GetMapping("/list/{page}/{limit}")
    public Result<Page<SkuInfo>>  listSkuInfo(@PathVariable("page") Long page,
                                              @PathVariable("limit") Long limit){

        Page<SkuInfo> infoPage = new Page<SkuInfo>(page,limit);

        Page<SkuInfo> skuInfoPage = skuInfoService.page(infoPage);
        return Result.ok(skuInfoPage);
    }
    @GetMapping("/onSale/{skuId}")
    public Result onSale(@PathVariable("skuId") Long skuId){

        boolean b = skuInfoService.onSale(skuId);
        return Result.ok();
    }


    @GetMapping("/cancelSale/{skuId}")
    public Result cancelSale(@PathVariable("skuId") Long skuId){
        boolean b = skuInfoService.cancelSale(skuId);
        return  Result.ok();
    }
}


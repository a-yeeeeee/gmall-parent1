package com.atguigu.gmall.product.service;

import com.atguigu.gmall.model.product.SpuSaleAttr;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author aye_quq
 * @create 2021-08-05 21:12
 */
public interface SpuSaleAttrService extends IService<SpuSaleAttr> {
    List<SpuSaleAttr> getSpuSaleAttrById(Long spuId);
}

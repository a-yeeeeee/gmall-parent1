package com.atguigu.gmall.product.service;

import com.atguigu.gmall.model.product.SkuAttrValue;
import com.atguigu.gmall.product.mapper.SkuAttrValueMapper;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author aye_quq
 * @create 2021-08-06 19:48
 */
public interface SkuAttrValueService extends IService<SkuAttrValue> {
}

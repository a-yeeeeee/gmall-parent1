package com.atguigu.gmall.product.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.model.product.BaseSaleAttr;
import com.atguigu.gmall.product.service.BaseSaleAttrInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author aye_quq
 * @create 2021-08-05 19:22
 */
@RestController
@RequestMapping("/admin/product")
public class BaseSaleAttrController {
    @Autowired
    private BaseSaleAttrInfoService baseSaleAttrInfoService;

    @GetMapping("/baseSaleAttrList")
    public Result<List<BaseSaleAttr>> getBaseSaleAttrList(){
        List<BaseSaleAttr> list = baseSaleAttrInfoService.list();
        return Result.ok(list);
    }

}

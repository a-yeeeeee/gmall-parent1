package com.atguigu.gmall.product.mapper;

import com.atguigu.gmall.model.product.BaseSaleAttr;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author aye_quq
 * @create 2021-08-05 19:25
 */
@Mapper
public interface BaseSaleAttrInfoMapper extends BaseMapper<BaseSaleAttr> {
}

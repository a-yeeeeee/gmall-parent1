package com.atguigu.gmall.product.mapper;

import com.atguigu.gmall.model.product.SpuImage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author aye_quq
 * @create 2021-08-05 21:06
 */
@Mapper
public interface SpuImageMapper extends BaseMapper<SpuImage> {
}

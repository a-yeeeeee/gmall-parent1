package com.atguigu.gmall.product.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.product.component.MinioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

/**
 * @author aye_quq
 * @create 2021-08-04 16:21
 */
@RestController
@RequestMapping("/admin/product")
public class FileUploadController {


    @Autowired
    MinioService minioService;

    //@RequestPart("file") MultipartFile file == MultipartFile file
    @PostMapping("/fileUpload")
    public Result<String> uploadFile(@RequestPart("file") MultipartFile file) {
        try{
            InputStream inputStream = file.getInputStream();
            String filename = file.getOriginalFilename();

            String upload = minioService.upload(inputStream, null, filename);
            return Result.ok(upload);
        }catch (Exception e){
            //e.printStackTrace();
            return Result.fail("网络异常");
        }
    }
}
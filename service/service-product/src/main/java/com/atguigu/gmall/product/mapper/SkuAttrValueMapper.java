package com.atguigu.gmall.product.mapper;

import com.atguigu.gmall.model.product.SkuAttrValue;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author aye_quq
 * @create 2021-08-06 19:47
 */
@Mapper
public interface SkuAttrValueMapper extends BaseMapper<SkuAttrValue> {
}

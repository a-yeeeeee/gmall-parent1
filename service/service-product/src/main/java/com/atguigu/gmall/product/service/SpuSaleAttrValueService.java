package com.atguigu.gmall.product.service;

import com.atguigu.gmall.model.product.BaseSaleAttr;
import com.atguigu.gmall.model.product.SpuSaleAttrValue;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author aye_quq
 * @create 2021-08-05 21:15
 */
public interface SpuSaleAttrValueService extends IService<SpuSaleAttrValue> {
}
